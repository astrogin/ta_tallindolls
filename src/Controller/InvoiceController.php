<?php

namespace App\Controller;

use App\Entity\Invoice;
use App\Resource\InvoiceResource;
use League\Csv\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InvoiceController extends AbstractController
{
    /**
     * @Route("/api/upload", name="invoice_upload")
     */
    public function index(Request $request)
    {
        $path = $request->files->get('csv')->getPathName();

        $csv = Reader::createFromPath($path);
        $csv->setHeaderOffset(0);

        $repository = $this->getDoctrine()->getRepository(Invoice::class);
        $repository->bulkInsert($csv);

        return $this->json(['message' => 'success'], 201);
    }

    /**
     * @Route("/api/invoices", name="invoices_list")
     */
    public function invoices()
    {
        return InvoiceResource::send(
            $this->getDoctrine()->getRepository(Invoice::class)->findAll()
        );
    }
}
