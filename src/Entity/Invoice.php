<?php

namespace App\Entity;

use App\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 */
class Invoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $internal_id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="date")
     */
    private $due_on_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInternalId(): ?string
    {
        return $this->internal_id;
    }

    public function setInternalId(string $internal_id): self
    {
        $this->internal_id = $internal_id;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDueOnDate(): ?\DateTimeInterface
    {
        return $this->due_on_date;
    }

    public function setDueOnDate(\DateTimeInterface $due_on_date): self
    {
        $this->due_on_date = $due_on_date;

        return $this;
    }

    public function getSellingPrice() 
    {
        //TODO: move coeff to const
        $coeff = $this->getDueOnDate()->diff(new \DateTime())->days <= 30 ? 0.3 : 0.5;
        return bcmul($this->getAmount(), $coeff, 2);
    }
}
