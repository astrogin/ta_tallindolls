<?php

namespace App\Resource;

use App\Entity\Invoice;
use Symfony\Component\HttpFoundation\JsonResponse;

class InvoiceResource 
{
    public static function send($invoices) 
    {
        $data = [];
        if ($invoices instanceof Invoice) {
            $data[] = [
                'internal_id' => $invoices->getInternalId(),
                'amount' => $invoices->getAmount(),
                'due_date' => $invoices->getDueOnDate(),
                'selling_price' => $invoices->getSellingPrice()
            ];
        }
        foreach($invoices as $invoice) {
            $data[] = [
                'internal_id' => $invoice->getInternalId(),
                'amount' => $invoice->getAmount(),
                'due_date' => $invoice->getDueOnDate(),
                'selling_price' => $invoice->getSellingPrice()
            ];
        }

        return new JsonResponse(['data' => $data]); 
    }
}
